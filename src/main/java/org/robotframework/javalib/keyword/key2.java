package org.robotframework.javalib.keyword;


import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;


@RobotKeywords
public class key2 {
    @RobotKeyword("Test Java")
    public void testJava() {
        System.out.println("My message is : java" );
    }

}