package org.robotframework.javalib.remote;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.robotframework.remoteserver.RemoteServer;
import org.robotframework.remoteserver.logging.Jetty2Log4J;

public class KeywordRemoteLibraryServer extends RemoteServer {

    public KeywordRemoteLibraryServer(String host, int port) {
        super(host, port);
    }

    private static Log log = LogFactory.getLog(RemoteServer.class);

    public static void configureLogging() {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.FATAL);
        org.eclipse.jetty.util.log.Log.setLog(new Jetty2Log4J());
        LogFactory.releaseAll();
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.Log4JLogger");
        log = LogFactory.getLog(RemoteServer.class);
    }

}
