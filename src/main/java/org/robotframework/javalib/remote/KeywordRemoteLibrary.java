package org.robotframework.javalib.remote;

import org.robotframework.javalib.library.AnnotationLibrary;

import java.util.ArrayList;
import java.util.List;

public class KeywordRemoteLibrary  extends AnnotationLibrary {

    static List<String> includePatterns = new ArrayList<String>() {{
        add("org/robotframework/javalib/keyword/*.class");
    }};

    public KeywordRemoteLibrary() {
        super(includePatterns);
    }
    public static void main(String[] args) throws Exception {

        String host = "127.0.1.1";
        int port = 64494;
        KeywordRemoteLibraryServer.configureLogging();
        KeywordRemoteLibraryServer server = new KeywordRemoteLibraryServer(host, port);
        server.putLibrary("/RPC2", new KeywordRemoteLibrary());
        server.start();
        System.out.println(new KeywordRemoteLibrary());
    }


}